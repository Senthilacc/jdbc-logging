package ca.css.log;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import javax.sql.DataSource;

import com.mchange.v2.c3p0.ComboPooledDataSource;

public class ConnectionFactory {
	
	private static interface Singleton {
		  final ConnectionFactory INSTANCE = new ConnectionFactory();
		}

		private final DataSource dataSource;

		private ConnectionFactory() {
			Properties prop = new Properties();
			InputStream input = null;
			ComboPooledDataSource ds = new ComboPooledDataSource();
			try {
				input = this.getClass().getResourceAsStream("/c3p0.properties");
				prop.load(input);
				ds.setDriverClass(prop.getProperty("c3p0.driverClass"));
				ds.setJdbcUrl(prop.getProperty("c3p0.jdbcUrl"));
			} catch (PropertyVetoException e) {
				e.printStackTrace();
			} 
			catch (IOException e) {
				e.printStackTrace();
			}
			 
			this.dataSource = ds;
		}

		public static Connection getDatabaseConnection() throws SQLException {
		   return Singleton.INSTANCE.dataSource.getConnection();
		}

}
